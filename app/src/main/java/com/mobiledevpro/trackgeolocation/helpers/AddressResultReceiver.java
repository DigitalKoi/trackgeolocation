package com.mobiledevpro.trackgeolocation.helpers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.os.ResultReceiver;

import com.mobiledevpro.trackgeolocation.models.event.EventResultReceiver;

import org.greenrobot.eventbus.EventBus;

public class AddressResultReceiver extends ResultReceiver {

    public AddressResultReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, final Bundle resultData) {
        if (resultCode == Constants.SUCCESS_RESULT) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new EventResultReceiver(
                            resultData.getString(Constants.RESULT_DATA_KEY)));
                }
            });
        }
    }
}
