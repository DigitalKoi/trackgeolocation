package com.mobiledevpro.trackgeolocation.helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobiledevpro.trackgeolocation.models.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class DatabaseHelper {

    private DatabaseReference mDatabase;

    private static DatabaseHelper sHelper;

    private DatabaseHelper() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public static DatabaseHelper getInstance() {
        if (sHelper == null) {
            sHelper = new DatabaseHelper();
        }
        return sHelper;
    }

    public void writeNewUser(String uId, String email, String publicName,
                             double latitude, double longitude, boolean online, String nameStreet) {
        User user = new User(email, publicName, latitude, longitude, online, nameStreet);
        Map<String, Object> userValue = user.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(uId, userValue);

        mDatabase.updateChildren(childUpdates);
    }

    public void addListenerFirebase(ValueEventListener listener) {
        mDatabase.addValueEventListener(listener);
    }

    public void removeListenerFirebase(ValueEventListener listener) {
        mDatabase.removeEventListener(listener);
    }

}
