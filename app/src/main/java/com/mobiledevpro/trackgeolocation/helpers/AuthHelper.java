package com.mobiledevpro.trackgeolocation.helpers;

import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mobiledevpro.trackgeolocation.R;
import com.mobiledevpro.trackgeolocation.models.event.EventMessageDialog;
import com.mobiledevpro.trackgeolocation.presenters.IAuthInterface;

import org.greenrobot.eventbus.EventBus;

public class AuthHelper implements GoogleApiClient.OnConnectionFailedListener {

    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInAccount mAccount;
    private IAuthInterface.View mView;
    private FirebaseAuth mAuth;

    public AuthHelper(IAuthInterface.View view) {
        mView = view;
    }

    public void configureGoogleSignIn() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    mView.startMainActivity();
                }
            }
        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mView.getContext().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(mView.getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    public void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        mView.showProgressBar();
        mAccount = account;
        AuthCredential credential = GoogleAuthProvider.getCredential(mAccount.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mView.hideProgressBar();

                        if (!task.isSuccessful()) {
                            callEventBus();
                        }
                    }
                });
    }

    public GoogleSignInAccount getAccount() {
        return mAccount;
    }

    public GoogleApiClient getSignIntent() {
        return mGoogleApiClient;
    }

    public void onBindFirebaseAuth() {
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void onUnbindFirebaseAuth() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void callEventBus() {
        EventBus.getDefault().post(new EventMessageDialog(
                R.string.msg_turn_internet,
                Settings.ACTION_SETTINGS));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        callEventBus();
    }
}
