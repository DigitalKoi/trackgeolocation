package com.mobiledevpro.trackgeolocation.presenters;

import android.app.Activity;

import com.google.firebase.database.ValueEventListener;
import com.mobiledevpro.trackgeolocation.models.User;

import java.util.List;

/**
 * Class for ...
 * <p>
 * Created by Dmitriy V. Chernysh on 31.01.17.
 * dmitriy.chernysh@gmail.com
 * <p>
 * www.mobile-dev.pro
 */

public interface IMainInterface {

    interface MainView {

        Activity getContext();

        void finishActivity();

        void showCoordinates(double latitude, double longitude);

        void setVisibleSignOut();

        void showUsers(List<User> users);

        void startIntentService(double latitude, double longitude);
    }

    interface Presenter {

        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        String getTitle();

        ValueEventListener getEventListener();

        void writeDatabase(boolean online);

        void setNameStreet(String nameStreet);

        void onStartView();

        void onStopView();

        void signOut();

        String getNameStreet(String title);

        String getNameStreetWithOutTitle();
    }
}
