package com.mobiledevpro.trackgeolocation.presenters;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public interface IAuthInterface {

    interface View {

        Activity getContext();

        void showProgressBar();

        void hideProgressBar();

        void startMainActivity();
    }

    interface Presenter {

        void onActivityResult(int requestCode, int resultCode, Intent data);

        GoogleApiClient getSignIntent();

        void onStartView();

        void onStopView();
    }
}
