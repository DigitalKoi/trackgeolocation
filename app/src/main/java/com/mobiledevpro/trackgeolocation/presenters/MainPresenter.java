package com.mobiledevpro.trackgeolocation.presenters;

import android.Manifest;
import android.app.PendingIntent;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mobiledevpro.locationservice.LocationServiceManager;
import com.mobiledevpro.trackgeolocation.App;
import com.mobiledevpro.trackgeolocation.R;
import com.mobiledevpro.trackgeolocation.helpers.Constants;
import com.mobiledevpro.trackgeolocation.helpers.DatabaseHelper;
import com.mobiledevpro.trackgeolocation.models.User;
import com.mobiledevpro.trackgeolocation.models.event.EventMessageDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Class for ...
 * <p>
 * Created by Dmitriy V. Chernysh on 31.01.17.
 * dmitriy.chernysh@gmail.com
 * <p>
 * www.mobile-dev.pro
 */

public class MainPresenter implements IMainInterface.Presenter {

    private LocationServiceManager.Callbacks mCallbacks;
    private ValueEventListener mEventListener;
    private DatabaseHelper mDatabaseHelper;
    private IMainInterface.MainView mView;
    private Map<String, String> hashmap;
    private String mNameStreet;
    private FirebaseAuth mAuth;
    private List<User> mUsers;
    private double mLongitude;
    private double mLatitude;

    public MainPresenter(IMainInterface.MainView view) {
        mDatabaseHelper = DatabaseHelper.getInstance();
        mView = view;
        mAuth = App.getFirebaseAuth();
        hashmap = new HashMap<>();
        authNotNull();
        mUsers = new ArrayList<>();
    }

    private void authNotNull() {
        if (mAuth.getCurrentUser() != null) {
            mView.setVisibleSignOut();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && PackageManager.PERMISSION_GRANTED == permissionCheck) {
            onStartView();
        } else {
            mView.finishActivity();
        }
    }

    @Override
    public String getTitle() {
        if (mAuth.getCurrentUser() != null) {
            writeDatabase(true);
            return mAuth.getCurrentUser().getDisplayName();
        }
        return "";
    }

    @Override
    public ValueEventListener getEventListener() {
        return mEventListener;
    }

    @Override
    public void writeDatabase(boolean online) {
        mDatabaseHelper.writeNewUser(
                mAuth.getCurrentUser().getUid(),
                mAuth.getCurrentUser().getEmail(),
                mAuth.getCurrentUser().getDisplayName(),
                mLatitude,
                mLongitude,
                online,
                mNameStreet);
    }

    @Override
    public void onStartView() {

        mEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUsers.clear();
                hashmap.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    User user = child.getValue(User.class);
                    mUsers.add(user);
                    hashmap.put(user.displayName, user.nameStreet);

                }
                if (mAuth.getCurrentUser() != null) {
                    mView.showUsers(mUsers);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mDatabaseHelper.addListenerFirebase(mEventListener);

        mCallbacks = new LocationServiceManager.Callbacks() {
            @Override
            public void isDeviceOffline() {
                EventBus.getDefault().post(new EventMessageDialog(
                        R.string.msg_turn_internet,
                        Settings.ACTION_SETTINGS));
            }

            @Override
            public void isNotLocationPermissionGranted() {
                ActivityCompat.requestPermissions(mView.getContext(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.PERMISSIONS_REQUEST_COARSE_LOCATION);
            }

            @Override
            public void onGoogleApiConnectionFailed(int errCode, String errMessage) {
                PendingIntent pendingIntent = LocationServiceManager.getGoogleApiErrorResolutionPendingIntent(
                        mView.getContext(),
                        errCode,
                        Constants.PERMISSIONS_REQUEST_RESULT_ACTIVITY /*for sending result to activity*/
                );

                if (pendingIntent != null) {
                    try {
                        pendingIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        //do nothing
                    }
                }
            }

            @Override
            public void onLocationUpdated(double latitude, double longitude, double altitude, float accuracy) {
                mLatitude = latitude;
                mLongitude = longitude;
                if (mView != null) {
                    mView.showCoordinates(latitude, longitude);
                    if (mAuth.getCurrentUser() != null) {
                        writeDatabase(true);
                    }
                    mView.startIntentService(latitude, longitude);
                }
            }

            @Override
            public void onGetLocationSettingsState(boolean isNetworkLocationOn, boolean isGpsLocationOn) {
                if (!isNetworkLocationOn && !isGpsLocationOn) {
                    EventBus.getDefault().post(new EventMessageDialog(
                            R.string.msg_turn_location,
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }
        };

        LocationServiceManager.getInstance().bindLocationService(mView.getContext(), mCallbacks);

    }

    @Override
    public void setNameStreet(String nameStreet) {
        nameStreet.replaceAll("\\n", ", ");
        mNameStreet = nameStreet.replaceAll("\\n", ", ");
        Log.d(TAG, "setNameStreet: " + mNameStreet);
        Log.d(TAG, "setNameStreet: ");
    }

    @Override
    public void onStopView() {
        LocationServiceManager.getInstance().unbindLocationService(mView.getContext());
        mDatabaseHelper.removeListenerFirebase(mEventListener);
    }

    @Override
    public void signOut() {
        writeDatabase(false);
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public String getNameStreet(String title) {
        return hashmap.get(title);
    }

    @Override
    public String getNameStreetWithOutTitle() {
        return mNameStreet;
    }
}
