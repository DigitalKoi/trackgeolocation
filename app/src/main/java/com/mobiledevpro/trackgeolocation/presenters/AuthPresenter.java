package com.mobiledevpro.trackgeolocation.presenters;

import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobiledevpro.trackgeolocation.helpers.AuthHelper;

import static com.mobiledevpro.trackgeolocation.helpers.Constants.RC_SIGN_IN;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class AuthPresenter implements
        IAuthInterface.Presenter {

    private final IAuthInterface.View mView;
    private AuthHelper mAuthHelper;

    public AuthPresenter(IAuthInterface.View view) {
        mView = view;
        mAuthHelper = new AuthHelper(mView);
        mAuthHelper.configureGoogleSignIn();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                mAuthHelper.firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(mView.getContext(), "System Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onStartView() {
        mAuthHelper.onBindFirebaseAuth();
    }

    @Override
    public void onStopView() {
        mAuthHelper.onUnbindFirebaseAuth();
    }

    @Override
    public GoogleApiClient getSignIntent() {
        return mAuthHelper.getSignIntent();
    }


}