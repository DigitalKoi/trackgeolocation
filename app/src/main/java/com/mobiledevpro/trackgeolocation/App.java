package com.mobiledevpro.trackgeolocation;

import android.app.Application;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Main app class
 * <p>
 * Created by Dmitriy V. Chernysh on 31.01.17.
 * dmitriy.chernysh@gmail.com
 * <p>
 * www.mobile-dev.pro
 */

public class App extends Application {
    private static App sApp;

    @Override
    public void onCreate() {
        super.onCreate();

        if (sApp == null) {
            sApp = this;
        }
    }

    /**
     * Get app context
     *
     * @return Context
     */
    public static Context getAppContext() {
        return sApp.getApplicationContext();
    }

    public static FirebaseAuth getFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }
}
