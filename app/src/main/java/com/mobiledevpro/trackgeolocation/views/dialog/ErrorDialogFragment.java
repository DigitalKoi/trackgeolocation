package com.mobiledevpro.trackgeolocation.views.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.mobiledevpro.trackgeolocation.R;
import com.mobiledevpro.trackgeolocation.models.event.EventServiceResult;

import org.greenrobot.eventbus.EventBus;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class ErrorDialogFragment extends DialogFragment {

    private int messageDialog;
    private String settingAction;
    public static final String ID_BUNDLE = "Id";
    public static final String MESSAGE_BUNDLE = "message";

    public static ErrorDialogFragment newInstance(int messageDialog, String settingAction) {
        ErrorDialogFragment fragment = new ErrorDialogFragment();

        Bundle args = new Bundle();
        args.putInt(ID_BUNDLE, messageDialog);
        args.putString(MESSAGE_BUNDLE, settingAction);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        messageDialog = bundle.getInt(ID_BUNDLE, 0);
        settingAction = bundle.getString(MESSAGE_BUNDLE, "");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(messageDialog);
        builder.setPositiveButton(R.string.btn_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EventBus.getDefault().post(new EventServiceResult(Activity.RESULT_OK, settingAction));
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EventBus.getDefault().post(new EventServiceResult(Activity.RESULT_CANCELED, settingAction));
            }
        });
        return builder.create();
    }
}
