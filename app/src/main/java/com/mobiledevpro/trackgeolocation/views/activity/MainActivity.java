package com.mobiledevpro.trackgeolocation.views.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobiledevpro.trackgeolocation.R;
import com.mobiledevpro.trackgeolocation.helpers.AddressResultReceiver;
import com.mobiledevpro.trackgeolocation.helpers.Constants;
import com.mobiledevpro.trackgeolocation.models.User;
import com.mobiledevpro.trackgeolocation.models.event.EventMessageDialog;
import com.mobiledevpro.trackgeolocation.models.event.EventResultReceiver;
import com.mobiledevpro.trackgeolocation.models.event.EventServiceResult;
import com.mobiledevpro.trackgeolocation.presenters.IMainInterface;
import com.mobiledevpro.trackgeolocation.presenters.MainPresenter;
import com.mobiledevpro.trackgeolocation.views.dialog.ErrorDialogFragment;
import com.mobiledevpro.trackgeolocation.views.service.GeocodeAddressIntentService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mobiledevpro.trackgeolocation.R.id.map;

public class MainActivity extends AppCompatActivity implements
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnMarkerClickListener,
        IMainInterface.MainView,
        OnMapReadyCallback {

    private AddressResultReceiver mResultReceiver;
    private IMainInterface.Presenter mPresenter;
    private boolean mShowMarker = true;
    private boolean mStartApp = true;
    private boolean mOffline = true;
    private LatLng mCurrentLocation;
    private Unbinder mUnbinder;
    private double mLongitude;
    private double mLatitude;

    private GoogleMap mMap;
    private BottomSheetDialog mBottomSheetDialog;
    @BindView(R.id.imgbtn_main_sign_out)
    ImageButton mImgBtnMainSignOut;
    private TextView nameTxt;
    private TextView locationTxt;
    private TextView latlonTxt;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUnbinder = ButterKnife.bind(this);
        mResultReceiver = new AddressResultReceiver(null);
        initializeViews();
    }

    private void initializeViews() {
        setGoneSignOut();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        mBottomSheetDialog = new BottomSheetDialog(getContext());
        View sheetView = getLayoutInflater().inflate(R.layout.buttom_sheet_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        nameTxt = (TextView) sheetView.findViewById(R.id.shdialog_txt_name);
        locationTxt = (TextView) sheetView.findViewById(R.id.shdialog_txt_location);
        latlonTxt = (TextView) sheetView.findViewById(R.id.shdialog_txt_latlon);
        mPresenter = new MainPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mPresenter.onStartView();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        verificationAuth();
        mPresenter.onStopView();
        super.onStop();
    }

    private void verificationAuth() {
        if (!mOffline) {
            mPresenter.writeDatabase(false);
        }
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnCameraMoveStartedListener(this);
    }

    @OnClick(R.id.ac_zoom_in)
    public void onZoomIn(ImageButton mImgBtnZoomIn) {
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
    }

    @OnClick(R.id.ac_zoom_out)
    public void onZoomOut(ImageButton mImgBtnZoomOut) {
        mMap.animateCamera(CameraUpdateFactory.zoomOut());
    }

    @OnClick(R.id.imgbtn_main_sign_out)
    public void signOut(ImageButton mImgBtnSignOut) {
        mOffline = true;
        mPresenter.signOut();
        refreshMarker();
        setGoneSignOut();
    }

    @Override
    public Activity getContext() {
        return this;
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @OnClick(R.id.ac_location_gps)
    public void getCurrentLocation(ImageButton mImgBtnLocation) {
        mCurrentLocation = new LatLng(mLatitude, mLongitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, 16.0f));
        mShowMarker = true;
    }

    @Override
    public void showCoordinates(double latitude, double longitude) {
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        if (latitude == 0 && longitude == 0) {
            mPresenter.onStartView();
        }
        mCurrentLocation = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, 16.0f));

        if (mOffline) {
            refreshMarker();
        }
        if (mStartApp) {
            mStartApp = false;
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrentLocation));
        }
        if (mShowMarker) {
            mMap.animateCamera(CameraUpdateFactory.newLatLng(mCurrentLocation));
        } else {
            mMap.stopAnimation();
        }
    }

    @Override
    public void setVisibleSignOut() {
        mImgBtnMainSignOut.setVisibility(View.VISIBLE);
    }

    private void setGoneSignOut() {
        mImgBtnMainSignOut.setVisibility(View.GONE);
    }

    private void refreshMarker() {
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(mCurrentLocation)
                .title(mPresenter.getTitle())).showInfoWindow();
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            if (mShowMarker) {
                mShowMarker = false;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showUsers(List<User> users) {
        mOffline = false;
        mMap.clear();
        for (User user : users) {
            if (user.online) {
                if (user.displayName.equals(mPresenter.getTitle())) {
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(user.latitude, user.longitude))
                            .title(user.displayName))
                            .showInfoWindow();
                } else {
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(user.latitude, user.longitude))
                            .title(user.displayName));
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        nameTxt.setText(marker.getTitle());
        latlonTxt.setText(marker.getPosition().latitude + ", " + marker.getPosition().longitude);
        if (marker.getTitle().equals("")) {
            locationTxt.setText(mPresenter.getNameStreetWithOutTitle());
        } else {
            locationTxt.setText(mPresenter.getNameStreet(marker.getTitle()));
        }
        mBottomSheetDialog.show();
        return true;
    }

    @Override
    public void startIntentService(double latitude, double longitude) {
        Intent intent = new Intent(this, GeocodeAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA,
                Double.parseDouble(String.valueOf(latitude)));
        intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA,
                Double.parseDouble(String.valueOf(longitude)));
        startService(intent);
    }

    @Subscribe
    public void onEvent(EventMessageDialog event) {
        createDialogBox(event.messageDialog, event.actionSettings);
    }

    private void createDialogBox(int messageDialog, String actionSettings) {
        DialogFragment dialogFragment = ErrorDialogFragment.newInstance(messageDialog, actionSettings);
        dialogFragment.setCancelable(false);
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Subscribe
    public void onEvent(EventServiceResult event) {
        showResultDialog(event.getResult(), event.getResultValue());
    }

    private void showResultDialog(int result, String resultValue) {
        if (result == 0) {
            finish();
        } else if (result == -1) {
            startActivity(new Intent(resultValue));
        }
    }

    @Subscribe
    public void onEvent(EventResultReceiver event) {
        mPresenter.setNameStreet(event.nameStreet);
    }

}
