package com.mobiledevpro.trackgeolocation.views.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.SignInButton;
import com.mobiledevpro.trackgeolocation.R;
import com.mobiledevpro.trackgeolocation.models.event.EventMessageDialog;
import com.mobiledevpro.trackgeolocation.models.event.EventServiceResult;
import com.mobiledevpro.trackgeolocation.presenters.AuthPresenter;
import com.mobiledevpro.trackgeolocation.presenters.IAuthInterface;
import com.mobiledevpro.trackgeolocation.views.dialog.ErrorDialogFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mobiledevpro.trackgeolocation.helpers.Constants.RC_SIGN_IN;

public class AuthActivity extends AppCompatActivity implements
        IAuthInterface.View {

    private IAuthInterface.Presenter mAuthPresenter;
    private Unbinder mUnbinder;
    private ProgressDialog mProgressDialog;

    @BindView(R.id.btn_auth_skip)
    Button mBtnMainSkip;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mUnbinder = ButterKnife.bind(this);
        mAuthPresenter = new AuthPresenter(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAuthPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btn_sign_in)
    public void signIn(SignInButton signInButton) {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mAuthPresenter.getSignIntent());
        startActivityForResult(intent, RC_SIGN_IN);
    }

    @OnClick(R.id.btn_auth_skip)
    public void skipAuth(Button button) {
        startMainActivity();
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Subscribe
    public void onEvent(EventMessageDialog event) {
        createDialogBox(event.messageDialog, event.actionSettings);
    }

    private void createDialogBox(int messageDialog, String actionSettings) {
        DialogFragment dialogFragment = ErrorDialogFragment.newInstance(messageDialog, actionSettings);
        dialogFragment.setCancelable(false);
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Subscribe
    public void onEvent(EventServiceResult event) {
        showResultDialog(event.getResult(), event.getResultValue());
    }

    private void showResultDialog(int result, String resultValue) {
        if (result == 0) {
            finish();
        } else if (result == -1) {
            startActivity(new Intent(resultValue));
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mAuthPresenter.onStartView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        mAuthPresenter.onStopView();
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showProgressBar() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(true);
        }
        mProgressDialog.setMessage(getString(R.string.msg_connection));
        mProgressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public Activity getContext() {
        return this;
    }
}