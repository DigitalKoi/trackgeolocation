package com.mobiledevpro.trackgeolocation.models.event;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class EventResultReceiver {
    public String nameStreet;

    public EventResultReceiver(String nameStreet) {
        this.nameStreet = nameStreet;
    }
}
