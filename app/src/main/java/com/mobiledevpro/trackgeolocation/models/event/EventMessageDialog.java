package com.mobiledevpro.trackgeolocation.models.event;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class EventMessageDialog {
    public int messageDialog;
    public final String actionSettings;

    public EventMessageDialog(int messageDialog, String actionSettings) {
        this.messageDialog = messageDialog;
        this.actionSettings = actionSettings;
    }
}
