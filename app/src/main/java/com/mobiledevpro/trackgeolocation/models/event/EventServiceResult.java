package com.mobiledevpro.trackgeolocation.models.event;

/**
 * @author KoiDev
 * @email DevSteelKoi@gmail.com
 */

public class EventServiceResult {

    public int result;

    public String resultValue;

    public EventServiceResult(int resultCode, String resultValue) {
        result = resultCode;
        this.resultValue = resultValue;
    }

    public int getResult() {
        return result;
    }

    public String getResultValue() {
        return resultValue;
    }
}
