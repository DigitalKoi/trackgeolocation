package com.mobiledevpro.trackgeolocation.models;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for ...
 * <p>
 * Created by Dmitriy V. Chernysh on 31.01.17.
 * dmitriy.chernysh@gmail.com
 * <p>
 * www.mobile-dev.pro
 */

@IgnoreExtraProperties
public class User {

    public String email;
    public String displayName;
    public double latitude;
    public double longitude;
    public boolean online;
    public String nameStreet;

    public User() {
    }

    public User(@NonNull String email, @NonNull String publicName,
                @NonNull double latitude, @NonNull double longitude,
                @NonNull boolean online, @NonNull String nameStreet) {
        this.email = email;
        this.displayName = publicName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.online = online;
        this.nameStreet = nameStreet;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("email", email);
        result.put("displayName", displayName);
        result.put("latitude", latitude);
        result.put("longitude", longitude);
        result.put("online", online);
        result.put("nameStreet", nameStreet);

        return result;
    }
}
